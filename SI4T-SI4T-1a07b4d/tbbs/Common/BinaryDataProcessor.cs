﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Tridion.ContentManager.ContentManagement;
using Tridion.ContentManager.Templating;
using Tridion.ContentManager.ContentManagement.Fields;

namespace SI4T.Templating.Common
{
    public class BinaryDataProcessor
    {
        public XmlDocument IndexData { get; set; }

        FieldProcessor _fieldProcessor;

        Engine _engine;
        Package _package;

        TemplatingLogger Logger = TemplatingLogger.GetLogger(typeof(BinaryDataProcessor));

        public BinaryDataProcessor()
        {
            IndexData = new XmlDocument();
            IndexData.LoadXml("<data><binaries></binaries></data>");
            _fieldProcessor = new FieldProcessor();
        }


        /// <summary>
        /// Initialize settings from templating package variables
        /// </summary>
        /// <param name="package">The templating package</param>
        public virtual void Initialize(Package package, Engine engine)
        {
            _engine = engine;
            _package = package;
        }


        /// <summary>
        /// Add Binary field values to the index data XML
        /// </summary>
        /// <param name="fieldName">Custom field name</param>
        /// <param name="values">values to add</param>
        /// <param name="encoded">set to true if values are already XML encoded (for example RTF content)</param>
        public virtual void SetBinaryValues(XmlDocument IndexData, string fieldName, List<object> values, bool encoded = false)
        {

            foreach (var val in values)
            {
                IndexData.SelectSingleNode("/*/*[local-name()='binaries']").AppendChild(_fieldProcessor.CreateElement(fieldName, encoded, val.ToString()));
            }
        }

        /// <summary>
        /// Add binary field value to the index data XML
        /// </summary>
        /// <param name="fieldName">Custom field name</param>
        /// <param name="values">value to add</param>
        /// <param name="encoded">set to true if values are already XML encoded (for example RTF content)</param>
        public virtual void SetBinaryFieldValue(XmlDocument IndexData, string fieldName, object value, bool encoded = false)
        {
            SetBinaryValues(IndexData, fieldName, new List<object> { value }, encoded);
        }

        /// <summary>
        /// Set custom index field values
        /// </summary>
        /// <param name="fieldName">custom field name</param>
        /// <param name="values">values for the index field</param>
        public virtual void SetBinaryFields(XmlDocument IndexData, string fieldName, List<object> values)
        {
            SetBinaryValues(IndexData, fieldName, values);
        }


        private List<Item> GetBinariesFromPackage()
        {
            var extensions = "pdf,jpg,png";
            Logger.Info("Looking for binaries in package with the following extensions: " + extensions);
            var binaryExtensions = (extensions ?? "").Split(',').Select(s => s.Trim()).ToList();
            List<Item> results = new List<Item>();
            //Item i.
            foreach (Item item in _package.GetAllByType(new ContentType("*/*")))
            {
                if (item.Properties.ContainsKey(Item.ItemPropertyTcmUri) && item.Properties.ContainsKey(Item.ItemPropertyFileName))
                {
                    var extension = Path.GetExtension(item.Properties[Item.ItemPropertyFileName]).Substring(1);
                    if (binaryExtensions.Contains(extension))
                    {
                        results.Add(item);

                    }
                }
            }
            return results;
        }


        public List<BinaryItem> ProcessBinaries()
        {
            Logger.Info("In Process Binaries");
            var binaryItems = GetBinariesFromPackage();
            Logger.Info("In Process Binaries: "+ binaryItems.Count);
            List<BinaryItem> binaries = new List<BinaryItem>();
            foreach (var item in binaryItems.Distinct())
            {
                var id = item.Properties[Item.ItemPropertyTcmUri];
                var binaryComponent = (Component)_engine.GetObject(id);
                var binaryId = id.Replace(":", "");
                int pos = binaryComponent.BinaryContent.Filename.LastIndexOf(".");
                var fileExtension = binaryComponent.BinaryContent.Filename.Substring(pos, binaryComponent.BinaryContent.Filename.Length - pos);
                var fileName = binaryComponent.BinaryContent.Filename.Replace(fileExtension, "_"+binaryId+ fileExtension);
                //byte[] fileBytes = binaryComponent.BinaryContent.GetByteArray();

                // string binarydata = Convert.ToBase64String(fileBytes, 0, fileBytes.Length);
                // File.WriteAllBytes(@"D:\Testfilecretaion\" + binaryComponent.BinaryContent.Filename, Convert.FromBase64String(binarydata));
                Logger.Info("Binary ID: "+id);
                Logger.Info("Binary Path: "+ binaryComponent.Path);

                BinaryItem binaryItem = new BinaryItem
                {
                    Title = binaryComponent.Title,
                    Id = binaryComponent.Id,
                    Url = binaryComponent.Path + fileName,
                   // binarydata = binarydata
                };
                binaries.Add(binaryItem);
            }
            return binaries;
        }
    }

    public class BinaryItem
    {
        public string Title { get; set; }
        public string Id { get; set; }
        public string Url { get; set; }
        public string binarydata { get; set; }
    }
}
